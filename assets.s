.include "libSFX.i"
.include "constants.i"
.rodata

incbin Palette, "assets/background.png.palette"
.export PaletteData = Palette, PaletteDataSize = sizeof_Palette

incbin BGTiles, "assets/background.png.tiles"
.export BGTileData = BGTiles, BGTileDataSize = sizeof_BGTiles

incbin SpriteTiles, "assets/sprites.png.tiles"
.export SpriteTileData = BGTiles, SpriteTileDataSize = sizeof_SpriteTiles

; Manual tilemap data for the background.
.proc BGTilemap
    .enum Flip
        FlipH    = $4000
        FlipV    = $8000
        FlipBoth = $C000
    .endenum

    .macro tile t, f
        .ifblank f
            .word (Tile::t)
        .else
            .word (Tile::t | Flip::f)
        .endif
    .endmacro

    ; Top 3 lines=blank
    .repeat (3 * 32)
        tile Blank
    .endrepeat

    ; Next line=blank, with top of bounding box
    .repeat 10
        tile Blank
    .endrepeat
    tile Corner, FlipBoth
    .repeat 10
        tile HBar, FlipV
    .endrepeat
    tile Corner, FlipV
    .repeat 10
        tile Blank
    .endrepeat

    ; Following 20 lines=sides of bounding box
    .repeat 20
        .repeat 10
            tile Blank
        .endrepeat
        tile VBar, FlipH
        .repeat 10
            tile Gray
        .endrepeat
        tile VBar
        .repeat 10
            tile Blank
        .endrepeat
    .endrepeat

    ; Next line=blank, with bottom of bounding box
    .repeat 10
        tile Blank
    .endrepeat
    tile Corner, FlipH
    .repeat 10
        tile HBar
    .endrepeat
    tile Corner
    .repeat 10
        tile Blank
    .endrepeat

    ; Last 3 lines=blank
    .repeat (3 * 32)
        tile Blank
    .endrepeat
.endproc

.export BGTilemap, BGTilemapSize = .sizeof(BGTilemap)

RotationBitmaps:
    .word $0F00, $2222, $00F0, $4444 ; I-piece
    .word $CC00, $CC00, $CC00, $CC00 ; O-piece
    .word $4E00, $4640, $0E40, $4C40 ; T-piece
    .word $8E00, $6440, $0E20, $44C0 ; J-piece
    .word $2E00, $4460, $0E80, $C440 ; L-piece
    .word $6C00, $4620, $06C0, $8C40 ; S-piece
    .word $C600, $2640, $0C60, $4C80 ; Z-piece
.export RotationBitmaps
