; Generic macro to declare a static variable.
.macro staticVar varName, varSize
.pushseg
.bss ; declare variable in BSS space
varName: .res varSize
.popseg ; go back to whatever space we were using before
.endmacro

; Generic macro to declare a static 2-byte variable.
.macro staticWord varName
    staticVar varName, 2
.endmacro

; Generic macro to declare a static 1-byte variable.
.macro staticByte varName
    staticVar varName, 1
.endmacro

; Left shift (multiply by 2) the A register count times.
.macro lshift count
    .repeat count
    asl
    .endrepeat
.endmacro

; Right shift (divide by 2) the A register count times.
.macro rshift count
    .repeat count
    lsr
    .endrepeat
.endmacro

.macro DecrementToZero word
    lda     word
    beq     :+
    dec
    sta     word
    :
.endmacro

.macro IncrementCircular word, max
    inc     word
    lda     word
    cmp     #(max)
    bne     :+
    stz     word
    :
.endmacro
