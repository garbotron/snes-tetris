FramesPerTurn = 30
FramesPerInput = 8
FramesPerDrop = 30

.enum VRAM
    BGTiles = $0000
    SpriteTiles = $1000
    BGMap = $2000
    PieceMap = $4000
.endenum

.enum Palette
    Gray
    Red
    Green
    Blue
    Violet
    Yellow
    Cyan
.endenum

.enum Tile
    Blank
    White
    Gray
    Black
    Outline
    Block
    VBar
    HBar
    Corner
    DeadBlock
.endenum

.enum Piece
    IPiece
    OPiece
    TPiece
    JPiece
    LPiece
    SPiece
    ZPiece
    Count
.endenum

.enum Rotation
    R0
    R90
    R180
    R270
    Count
.endenum
