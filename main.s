.include "libSFX.i"
.include "macros.i"
.include "constants.i"

staticWord curX         ; Current column index of the held piece (top-left)
staticWord curY         ; Current line index of the held piece (top-left)
staticWord curPiece     ; Currently held tetromino
staticWord curRotation  ; Currently held tetromino rotation
staticWord xMoveDelay   ; Number of frames until we are allowed to move x again
staticWord rotateDelay  ; Number of frames until we are allowed to rotate again
staticWord dropDelay    ; Number of frames until we are allowed to drop again
staticWord dropBlocked  ; Flag to indicate that we a piece hit the bottom or another block in the last frame
staticWord turnCounter  ; Number of frames before we're done with a "turn" (automatic downward movement)
staticWord temp         ; Generic temporary variable used by various functions

; Full map of the entire screen area, 1 word per tile. Used to track game state.
; [0] = is the tile full? (out of the grid = true).
; [3:1] = palette index of the block (irrelevant if bit 0 is not set).
staticVar tileStates, 32 * 32 * 2

; Input:  X = X offset of the tile.
;         Y = Y offset of the tile.
; Output: X = offset into the tileStates table.
.macro CalculateTileStatesOffset
        ; Offset into the tile state table = ((y * 32) + x) * 2 = (y * 64) + (x * 2).
        tya
        lshift  6
        sta     temp
        txa
        lshift  1
        add     temp
        tax
.endmacro

.macro InitPalette idx, rDiv, gDiv, bDiv
        ; The color palette format is 2 bytes: ggg[lo]rrrrr _bbbbbgg[hi]
        ; We will unpack this into its RGB components then divide the chosen channels by 2.
        lda     #(idx)
        sta     CGADD

        stz     entry
:       lda     entry
        lshift  1
        tax

        ; Unpack the data into 3 channels. This requires a lot of bitmasking and shifting.
        lda     PaletteData, x
        rshift  5
        sta     greenChannel
        lda     PaletteData, x
        and     #$1F
        sta     redChannel
        inx
        lda     PaletteData, x
        rshift  2
        and     #$1F
        sta     blueChannel
        lda     PaletteData, x
        and     #$03
        lshift  3
        ora     greenChannel
        sta     greenChannel

        ; Process the color channels according to the macro arguments.
        .if .match(rDiv, Y)
        lsr     redChannel
        .endif
        .if .match(gDiv, Y)
        lsr     greenChannel
        .endif
        .if .match(bDiv, Y)
        lsr     blueChannel
        .endif

        ; Re-pack the data into the CGRAM data port.
        lda     greenChannel
        lshift  5
        ora     redChannel
        sta     CGDATA
        lda     blueChannel
        lshift  2
        sta     temp
        lda     greenChannel
        rshift  3
        ora     temp
        sta     CGDATA

        ; We're done with this entry, move to the next one.
        lda     entry
        inc     a
        sta     entry
        cmp     #16
        bne     :-
.endmacro

.macro InitBothPalettes idx, rDiv, gDiv, bDiv
        InitPalette (idx * 16), rDiv, gDiv, bDiv         ; BG
        InitPalette ((idx * 16) + 128), rDiv, gDiv, bDiv ; Sprites
.endmacro

proc InitSystem, a8
        ; Clear out the high-order byte of register A (AKA register B).
        lda     #0
        xba

        ; Initialize CGRAM (palette). The asset file is in grayscale, and we shift the colors here manually.
        staticByte entry
        staticByte redChannel
        staticByte blueChannel
        staticByte greenChannel
        InitBothPalettes 0, N, N, N
        InitBothPalettes 1, N, N, Y
        InitBothPalettes 2, N, Y, N
        InitBothPalettes 3, Y, N, N
        InitBothPalettes 4, N, Y, Y
        InitBothPalettes 5, Y, N, Y
        InitBothPalettes 6, Y, Y, N

        ; Initialize VRAM (background tiles/map).
        VRAM_memcpy VRAM::BGMap, BGTilemap, BGTilemapSize
        VRAM_memset VRAM::PieceMap, BGTilemapSize, 0
        VRAM_memcpy VRAM::BGTiles, BGTileData, BGTileDataSize
        VRAM_memcpy VRAM::SpriteTiles, SpriteTileData, SpriteTileDataSize

        ; Set up screen mode (mode 1, BG1, BG2 and object layer enabled).
        lda     #bgmode(BG_MODE_1, BG3_PRIO_NORMAL, BG_SIZE_8X8, BG_SIZE_8X8, BG_SIZE_8X8, BG_SIZE_8X8)
        sta     BGMODE
        lda     #bgsc(VRAM::PieceMap, SC_SIZE_32X32)
        sta     BG1SC
        lda     #bgsc(VRAM::BGMap, SC_SIZE_32X32)
        sta     BG2SC
        lda     #bgnba(VRAM::BGTiles, 0, 0, 0)
        sta     BG12NBA
        lda     #objsel(VRAM::SpriteTiles, OBJ_8x8_16x16, 0)
        ldx     OBJSEL
        lda     #tm(ON, ON, OFF, OFF, ON)
        sta     TM

        ; Set VBlank handler.
        VBL_set VBlank

        ; Turn on screen.
        lda     #inidisp(ON, DISP_BRIGHTNESS_MAX)
        sta     SFX_inidisp
        VBL_on

        rts
endproc

proc PrepareNewPiece, a16
        ; Start at X, Y = top left corner of the grid.
        lda     #11
        sta     curX
        lda     #3 ; 1 line above the top of the grid
        sta     curY

        ; Random number generators are annoying, so do some silliness with the frame counter to look kinda random.
        lda     curPiece
        inc     a
        eor     SFX_tick
        ; Use the HW math data port to do an unsigned divison.
        tax
        RW_push set:a8
        divu    x, Piece::Count, x, y
        RW_pull
        sty     curPiece
        rts
endproc

proc InitGame, a16
        ; Fill the initial tile state map, setting the fill bit for each tile that's on the border of the grid.
        ; (we don't bother with above the grid since you can't ever get a piece up there)
        lda     #1
        .repeat 32, ty ; left side
        ldx     #(((ty * 32) + 10) * 2)
        sta     tileStates, x
        .endrepeat
        .repeat 32, ty ; right side
        ldx     #(((ty * 32) + 21) * 2)
        sta     tileStates, x
        .endrepeat
        .repeat 32, tx ; bottom
        ldx     #(((24 * 32) + tx) * 2)
        sta     tileStates, x
        .endrepeat

        jsr     PrepareNewPiece
        rts
endproc

proc LoadRotationBitmap, a16
        ; Offset into the roation bitmap tables = (piece * 8) + (rotation * 2).
        lda     curPiece
        lshift  3
        add     curRotation
        add     curRotation
        tax
        lda     RotationBitmaps, x
        rts
endproc

proc IsXYBlocked, a16
        ; Load the tile state for the tile (reg X, reg Y).
        CalculateTileStatesOffset
        lda     tileStates, x

        ; Check if bit [0] is set.
        and     #1
        beq     :+
        lda     #1
        rts

:       lda     #0 ; we are in bounds
        rts
endproc

; Output: A=1 if the the current X/Y/piece/rotation has any overlap with existing grid cells, else A=0.
proc IsCurStateOverlapping, a16
        staticWord rotBmp
        jsr     LoadRotationBitmap
        sta     rotBmp

        .repeat 4, ty
        .repeat 4, tx

        asl     rotBmp
        bcc     :+ ; a little ugly, but named labels don't play nice with .repeat

        lda     curX ; load reg x with the current X value + the X offset of the tile
        add     #tx
        tax
        lda     curY ; load reg y with the current Y value + the Y offset of the tile
        add     #ty
        tay
        jsr     IsXYBlocked ; sets A=1 if the tile is open, else A=0
        cmp     #1
        bne     :+
        lda     #1 ; return A=1 since this state is not valid
        rts

:       .endrepeat
        .endrepeat

        lda     #0 ; return A=0 since this state is valid
        rts
endproc

.macro WriteOAMData
        RW_push set:a8  ; Use 8-bit math for interaction with the OAM bus.
        sta     OAMDATA ; write to the bus
        RW_pull
.endmacro

; Input:
; A = starting sprite index
; X = tile index of block tile to use
; Y = palette index
proc UpdatePieceSprites, a16
        staticWord startIdx
        staticWord blockTile
        staticWord blockPalette
        staticWord rotBmp

        sta     startIdx
        stx     blockTile
        sty     blockPalette

        jsr     LoadRotationBitmap
        sta     rotBmp

        ; Don't draw the topmost row if it is off the top of the screen.
        lda     curY
        cmp     #3
        bne     :+
        lda     rotBmp
        and     #$0FFF
        sta     rotBmp

        ; Now to update the sprites in OAM memory. This is a somewhat complex operation using data port registers.
:       lda     startIdx
        sta     OAMADDL ; Start at the given OAM address. Each write to the data port increments this addr.

        .repeat 4, ty
        .repeat 4, tx

        ; Rotate the bitmap to the left. If the previous high bit was a 1, the tile should be visible.
        asl     rotBmp
        bcs     :+ ; we can't use named labels here since we're inside a .repeat directive

        ; If we got here, it means the tile shouldn't be visible (move it off the screen).
        RW_push set:a8
        lda     #$FF
        WriteOAMData
        WriteOAMData
        lda     #$00
        WriteOAMData
        WriteOAMData
        RW_pull
        bra     :++

:       lda     curX    ; load the current x coord from memory
        add     #tx     ; add the x increment
        lshift  3       ; multiply by 8 to get the pixel
        WriteOAMData    ; write to the bus
        lda     curY    ; load the current y coord from memory
        add     #ty     ; add the y increment
        lshift  3       ; multiply by 8 to get the pixel
        WriteOAMData    ; write to the bus
        lda     blockTile
        WriteOAMData

        ; Finally set the palette for this piece, along with priority 3 relative to BG.
        lda     blockPalette
        lshift  1
        ora     #(3 << 4)
        WriteOAMData

:       .endrepeat
        .endrepeat

        rts
endproc

; Increment curY until we hit something.
proc DropY, a16
down:   inc     curY
        jsr     IsCurStateOverlapping
        cmp     #1
        bne     down
        dec     curY
        rts
endproc

proc UpdateDropGhostSprites, a16
        ; Save the old Y coordinate.
        staticWord oldY
        lda     curY
        sta     oldY

        ; Scan downward to find the drop target.
        jsr     DropY

        ; Update the sprites.
        lda     #64
        ldx     #Tile::Outline
        ldy     #0
        jsr     UpdatePieceSprites

        ; Restore the old Y coordinate.
        lda     oldY
        sta     curY
        rts
endproc

proc PerformRotate, a16
        staticWord prevRotation
        lda     rotateDelay
        bne     done
        lda     curRotation
        sta     prevRotation
        IncrementCircular curRotation, Rotation::Count
        jsr     IsCurStateOverlapping
        cmp     #1
        bne     ok
ng:     lda     prevRotation
        sta     curRotation
        bra     done
ok:     lda     #FramesPerInput
        sta     rotateDelay
done:   rts
endproc

proc PerformDrop, a16
        lda     dropDelay
        bne     done
        lda     #FramesPerDrop
        sta     dropDelay
        jsr     DropY       ; move curY downward until we hit something
        lda     #1
        sta     turnCounter ; immediately lock the pieces and go to the next turn
done:   rts
endproc

proc MoveLeft, a16
        lda     xMoveDelay
        bne     done
        dec     curX
        jsr     IsCurStateOverlapping
        cmp     #1
        bne     ok
ng:     inc     curX
        bra     done
ok:     lda     #FramesPerInput
        sta     xMoveDelay
done:   rts
endproc

proc MoveRight, a16
        lda     xMoveDelay
        bne     done
        inc     curX
        jsr     IsCurStateOverlapping
        cmp     #1
        bne     ok
ng:     dec     curX
        bra     done
ok:     lda     #FramesPerInput
        sta     xMoveDelay
done:   rts
endproc

proc ProcessFrame, a16
        ; Controller bit mapping:
        ; Bit       Button
        ; 15        B
        ; 14        Y
        ; 13        Select
        ; 12        Start
        ; 11        Up
        ; 10        Down
        ; 09        Left
        ; 08        Right
        ; 07        A
        ; 06        X
        ; 05        L
        ; 04        R

        ; Decrement the delay counters for moving by X, rotation, and drops (separate delay timers).
        DecrementToZero xMoveDelay
        DecrementToZero rotateDelay
        DecrementToZero dropDelay

        rol     SFX_joy1cont ; B
        rol     SFX_joy1cont ; Y
        rol     SFX_joy1cont ; Select
        rol     SFX_joy1cont ; Start
        rol     SFX_joy1cont ; Up
        bcc     :+
        jsr     PerformRotate
:       rol     SFX_joy1cont ; Down
        bcc     :+
        jsr     PerformDrop
:       rol     SFX_joy1cont ; Left
        bcc     :+
        jsr     MoveLeft
:       rol     SFX_joy1cont ; Right
        bcc     :+
        jsr     MoveRight
:       rts
endproc

proc ProcessTurn, a16
        inc     curY
        ; If we hit something (a block or the ground), record it so we can act on it in the next VBlank period.
        jsr     IsCurStateOverlapping
        sta     dropBlocked
        beq     :+
        dec     curY
:       rts
endproc

proc StickPieceInTileState, a16
        staticWord rotBmp
        jsr     LoadRotationBitmap
        sta     rotBmp

        .repeat 4, ty
        .repeat 4, tx

        asl     rotBmp
        bcc     :+ ; a little ugly, but named labels don't play nice with .repeat
        lda     curX ; load reg x with the current X value + the X offset of the tile
        add     #tx
        tax
        lda     curY ; load reg y with the current Y value + the Y offset of the tile
        add     #ty
        tay
        CalculateTileStatesOffset
        lda     curPiece
        lshift  1
        ora     #1
        sta     tileStates, x

:       .endrepeat
        .endrepeat

        rts
endproc

proc StickPieceInBG, a16
        staticWord rotBmp
        jsr     LoadRotationBitmap
        sta     rotBmp

        .repeat 4, ty
        .repeat 4, tx

        asl     rotBmp
        bcc     :+ ; a little ugly, but named labels don't play nice with .repeat

        ; The VRAM address to write to is: ((VRAM::Map / 2) + (32 * (curY + ty)) + curX + tx)
        lda     curY
        add     #ty
        lshift  5
        add     curX
        add     #(tx + (VRAM::PieceMap / 2))
        sta     VMADDL

        ; The VRAM data to write is: ((palette << 10) | sprite).
        lda     curPiece ; palette ID == piece ID
        lshift  10
        add     #Tile::DeadBlock
        sta     VMDATAL

:       .endrepeat
        .endrepeat

        rts
endproc

; Input: Y=row
; Clears the line at index Y in VRAM as well as the internal state. Move all lines above Y downward.
proc ClearLine, a16
        staticWord curLine
        staticWord tempState

        ; Starting from 4, down to the indicated Y index, copy the lines downward.
        sty     curLine
loop:   jsr     CopyLineFromAbove
        dec     curLine
        lda     #3
        cmp     curLine
        bne     loop
        rts

        proc CopyLineFromAbove, a16
                ; Load the VRAM address for line Y.
                lda     curLine
                lshift  5
                add     #(11 + (VRAM::PieceMap / 2))
                sta     VMADDL

                ; Load the software state offset for line Y-1 (source line) into tempState.
                ldx     #11
                ldy     curLine
                dey
                CalculateTileStatesOffset
                stx     tempState

                ; Load the software state offset for line Y (destination line) into Y.
                ldx     #11
                ldy     curLine
                CalculateTileStatesOffset
                txy

                ; Copy tempState (Y-1 state) back to X.
                ldx     tempState

                ; Now we can copy all of the data downward.
                .repeat 10

                ; Copy the tile data from offset X to Y.
                lda     tileStates, x
                sta     tileStates, y

                ; Copy the VRAM data ((palette << 10) | sprite) based on the source tile state.
                lda     tileStates, x
                rshift  1
                bcs     :+
                lda     #Tile::Gray
                bra     :++
:               lshift  10
                ora     #Tile::DeadBlock
:               sta     VMDATAL

                ; Increment the source/dest tile in the tile data map.
                inx
                inx
                iny
                iny

                .endrepeat

                rts
        endproc
endproc

; Input: Y=row
; Output: A=1 if line should be cleared, a=0 otherwise.
proc ShouldLineClear, a16
.repeat 10, tx
        ldx     #(tx + 11)
        CalculateTileStatesOffset
        lda     tileStates, x
        bne     :+
        lda     #0
        rts
:
.endrepeat
        lda     #1
        rts
endproc

proc ClearCompletedLines, a16
.repeat 20, ty
        ldy     #(ty + 4)
        jsr     ShouldLineClear
        cmp     #1
        bne     :+
        jsr     ClearLine
:
.endrepeat
        rts
endproc

proc UpdateForDropBlocked, a16
        jsr     StickPieceInTileState
        jsr     StickPieceInBG
        jsr     ClearCompletedLines
        jsr     PrepareNewPiece
        rts
endproc

proc Main, a8
        jsr     InitSystem
        RW_push set:a16
        jsr     InitGame
turn:   lda     #FramesPerTurn
        sta     turnCounter
frame:  wai     ; handle vblank
        jsr     ProcessFrame
        dec     turnCounter
        bne     frame
        jsr     ProcessTurn
        bra     turn
        RW_pull
endproc

proc VBlank, a8
        RW_push set:a16
        lda     #0
        ldx     #Tile::Block
        ldy     curPiece ; palette ID = piece ID
        jsr     UpdatePieceSprites
        jsr     UpdateDropGhostSprites
        lda     dropBlocked
        beq     :+
        jsr     UpdateForDropBlocked
        stz     dropBlocked
:       rtl
        RW_pull
endproc
