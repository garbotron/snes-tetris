name := snes-tetris
debug := 1
libsfx_dir := ../libSFX
dist_dir := /mnt/c/Temp

# Note that the background and sprite layers are identical (symlinked).
# I generate separate palettes for them just to play nice with libSFX.
derived_files += assets/sprites.png.palette
derived_files += assets/sprites.png.tiles
derived_files += assets/background.png.palette
derived_files += assets/background.png.tiles

assets/sprites.png.tiles: tiles_flags=--sprite-mode

include $(libsfx_dir)/libSFX.make

$(dist_dir)/$(rom): $(rom)
	cp $< $@

default: $(dist_dir)/$(rom)
